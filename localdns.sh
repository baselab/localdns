#!/bin/bash

shopt -s extglob

iface="eth0"
port=53
server=$(ip -f inet -o address show dev ${iface} | sed 's/^.*inet //; s/\/[0-9][0-9]* .*//')
network="${server/%\.+([0-9])/}"
start_addr="${network}.60"
end_addr="${network}.70"
domain="localdomain"
hostsfile="localdns.hosts"

if [[ -f "${hostsfile}" ]] ; then
    hostsfileopt="--dhcp-hostsfile=${hostsfile}"
fi

dnsCmd="dnsmasq"

dnsOpt="-z \
    -p ${port} \
    -a ${server} \
    -i ${iface} \
    -D \
    --dhcp-range=${start_addr},${end_addr} \
    --dhcp-option=3,${server} \
    --dhcp-option=6,${server} \
    --dhcp-option=15,${domain} \
    ${hostsfileopt}"

dnsStart() {
    sudo ${dnsCmd} ${dnsOpt}
}

dnsStop() {
    sudo pkill -f "${dnsCmd}.*${iface}"
}

case ${1} in
    start)
        dnsStart
    ;;
    stop)
        dnsStop
    ;;
    restart)
        dnsStop
        sleep 1
        dnsStart
    ;;
    *)
        echo "Usage: $(basename ${0}) {start|stop|restart}"
        exit 1
    ;;
esac
