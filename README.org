* Simple local DNS
/simple local dns service, useful for a temporary or testing virtual
environment/

** Dependencies
- ~dnsmasq~
- ~iproute2~

** Configuration
Configure at least:
- ~${iface}~
- ~${start_addr}~
- ~${end_addr}~

The script assumes that the network interface is already configured with
one IP address.

If you need static MAC-IP, edit ~localdns.hosts.example~ and rename it
~localdns.hosts~.

Remember that dnsmaq reads ~/etc/hosts~ and serves those names by default.
This is useful if you want to blacklist some domains.

IPv6 is not suported.

** Usage
: localdns {start|stop|restart}
